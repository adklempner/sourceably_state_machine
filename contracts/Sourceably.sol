pragma solidity ^0.4.18;

import "zeppelin-solidity/contracts/ownership/rbac/RBAC.sol";
import "./StateMachine.sol";

contract Sourceably is RBAC, StateMachine {

  struct StateChange {
    bool enabled;
  	string role; // who is authorized to make this state change
  	uint signaturesRequired;//  - # of authorizations needed
  	/* oraclesRequired - # of oracles this state change needs
  	oracleIds[] - oracle references */
  	/* uint filesRequired; // - # of files this state change needs
  	bytes32[] filesHashes;// - ipfs hashes */


    uint block; // last block this state change was updated
    bool outbound; // this state change requires a call to another machine to complete
  }

  //typeid => currState => nextState => StateChange
  mapping(bytes32 =>
  	mapping(bytes32 => mapping(bytes32
  			  => StateChange))
  ) validStateChangesByProduct;

  //cycleId => currState
  mapping(bytes32 => bytes32) productCycles;

  function performStateChange(bytes32 typeId, bytes32 cycleId, bytes32 nextState) returns (bool) {
    require(isValidStateChange(typeId, productCycles[cycleId], nextState));
    require(!validStateChangesByProduct[typeId][productCycles[cycleId]][nextState].outbound);
    checkRole(msg.sender, validStateChangesByProduct[typeId][productCycles[cycleId]][nextState].role);
    StateChanged(cycleId, productCycles[cycleId], nextState, msg.sender);
    productCycles[cycleId] = nextState;
    return true;
  }

  function performStateChangeMachineTransition(bytes32 typeId, bytes32 cycleId, bytes32 nextState, bytes32 machineTypeId, bytes32 machineCycleId, bytes32 machineNextState, address machine) {
    require(isValidStateChange(typeId, productCycles[cycleId], nextState));
    require(validStateChangesByProduct[typeId][productCycles[cycleId]][nextState].outbound);
    //Perform state change on home machine as normal
    checkRole(msg.sender, validStateChangesByProduct[typeId][productCycles[cycleId]][nextState].role);
    StateChanged(cycleId, productCycles[cycleId], nextState, msg.sender);
    productCycles[cycleId] = nextState;
    require(StateMachine(machine).performStateChange(machineTypeId, machineCycleId, machineNextState));
  }

  function addStateChange(bytes32 typeId, bytes32 currState, bytes32 nextState, string _role, uint _signaturesRequired, bool _outbound) onlyAdmin {
    require(!validStateChangesByProduct[typeId][currState][nextState].enabled);
    validStateChangesByProduct[typeId][currState][nextState].enabled = true;
    validStateChangesByProduct[typeId][currState][nextState].role = _role;
    validStateChangesByProduct[typeId][currState][nextState].signaturesRequired = _signaturesRequired;
    validStateChangesByProduct[typeId][currState][nextState].outbound = _outbound;
    StateChangeAdded(typeId, currState, nextState, msg.sender);
  }

  function removeStateChange(bytes32 typeId, bytes32 currState, bytes32 nextState) onlyAdmin {
    require(validStateChangesByProduct[typeId][currState][nextState].enabled);
    delete validStateChangesByProduct[typeId][currState][nextState];
    StateChangeRemoved(typeId, currState, nextState, msg.sender);
  }

  function isValidStateChange(bytes32 typeId, bytes32 currState, bytes32 nextState) public constant returns(bool) {
    return validStateChangesByProduct[typeId][currState][nextState].enabled;
  }

  event StateChangeAdded(bytes32 productTypeId, bytes32 currState, bytes32 nextState, address user);
  event StateChangeRemoved(bytes32 productTypeId, bytes32 currState, bytes32 nextState, address user);
  event StateChanged(bytes32 cycleId, bytes32 prevState, bytes32 newState, address user);

}
