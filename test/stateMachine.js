const assert = require('chai').assert;
const abiDecoder = require('abi-decoder');

const Sourceably = artifacts.require('Sourceably.sol');

abiDecoder.addABI(Sourceably._json.abi);

const isInvalidOpcodeEx = function(e) {
    return e.message.search('invalid opcode') >= 0;
}

contract('Sourceably', (accounts) => {

  let stateMachine;

  before(async () => {
    stateMachine = await Sourceably.new();
  });

  describe('Creating State Changes', async () => {
    let typeId = web3.toHex('BACON');
    let currState = web3.toHex('RECEIVE_SHIPMENT');
    let nextState = web3.toHex('PROCESS_SHIPMENT');
    let branchState = web3.toHex('HOLD_SHIPMENT');
    let shipState = web3.toHex('SHIPPER_CLAIMS_SHIPPED');
    let roleId = "admin";
    let signaturesRequired = 1;
    let admin = accounts[0];
    let manager = accounts[1];
    let handler = accounts[2];

    let shipperAdmin = accounts[3];

    it('can add a new state change', async () => {
      let tx = await stateMachine.addStateChange(typeId, currState, nextState, roleId, signaturesRequired, false);
      console.log('Transaction hash');
      console.log(tx.tx);
      console.log('Transaction receipt');
      console.log(web3.eth.getTransactionReceipt(tx.tx));
      const events = abiDecoder.decodeLogs(tx.receipt.logs);
      console.log('Decoded logs');
      console.log(events[0]);
      console.log('Events from logs');
      console.log(events[0].events);

      assert(await stateMachine.isValidStateChange(typeId, currState, nextState));
    });

    it('can remove a state change', async() => {
      let tx = await stateMachine.removeStateChange(typeId, currState, nextState);
      console.log('Transaction hash');
      console.log(tx.tx);
      console.log('Transaction receipt');
      console.log(web3.eth.getTransactionReceipt(tx.tx));
      const events = abiDecoder.decodeLogs(tx.receipt.logs);
      console.log('Decoded logs');
      console.log(events[0]);
      console.log('Events from logs');
      console.log(events[0].events);

      assert(!(await stateMachine.isValidStateChange(typeId, currState, nextState)));
    });

    it('can add an initial state and start a new cycle', async() => {
      let cycleId = 0;
      await stateMachine.addStateChange(typeId, 0, currState, roleId, signaturesRequired, false);
      let tx = await stateMachine.performStateChange(typeId, 0, currState);
      console.log('Transaction hash');
      console.log(tx.tx);
      console.log('Transaction receipt');
      console.log(web3.eth.getTransactionReceipt(tx.tx));
      const events = abiDecoder.decodeLogs(tx.receipt.logs);
      console.log('Decoded logs');
      console.log(events[0]);
      console.log('Events from logs');
      console.log(events[0].events);
      await stateMachine.addStateChange(typeId, currState, nextState, roleId, signaturesRequired, false);
      await stateMachine.performStateChange(typeId, 0, nextState);
    })

    it('cannot perform an invalid state change', async() => {
      try {
        await stateMachine.performStateChange(typeId, 0, nextState);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      try {
        await stateMachine.performStateChange(typeId, 0, currState);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      try {
        await stateMachine.performStateChange(typeId, 1, nextState);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      await stateMachine.performStateChange(typeId, 1, currState);

      try {
        await stateMachine.performStateChange(typeId, 1, currState);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      try {
        await stateMachine.performStateChange(typeId, 1, 0);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      await stateMachine.performStateChange(typeId, 1, nextState);
    })

    it('only admin can add and remove state changes', async() => {
      try {
        await stateMachine.addStateChange(typeId, currState, branchState, roleId, signaturesRequired, false, {from: manager});
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      try {
        await stateMachine.removeStateChange(typeId, currState, nextState, {from: manager});
      } catch(e) {
        isInvalidOpcodeEx(e);
      }
    })

    it('address without correct role cannot perform state change', async() => {
      try {
        await stateMachine.performStateChange(typeId, 2, currState);
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      await stateMachine.addStateChange(typeId, nextState, branchState, "manager", signaturesRequired, false, {from: admin});
      await stateMachine.adminAddRole(manager, "manager", {from: admin});
      try {
        await stateMachine.performStateChange(typeId, 1, branchState, {from: admin});
      } catch(e) {
        isInvalidOpcodeEx(e);
      }

      await stateMachine.performStateChange(typeId, 1, branchState, {from: manager});
    })

    it('other state machine can perform an approved state transition', async() => {
      let shipperMachine = await Sourceably.new({from: shipperAdmin});

      //add shipper role to shipper machine
      await stateMachine.adminAddRole(shipperMachine.address, "shipper");
      //add state change only shipper can make
      await stateMachine.addStateChange(typeId, 0, shipState, "shipper", signaturesRequired, false);
      //add outbound state change to shipper machine
      await shipperMachine.addStateChange(typeId, 0, web3.toHex('SHIPPED_OUT_TO_PRODUCER'), "admin", signaturesRequired, true, {from: shipperAdmin});
      //bytes32 typeId, bytes32 cycleId, bytes32 nextState, bytes32 machineTypeId, bytes32 machineCycleId, bytes32 machineNextState, address machine
      let tx = await shipperMachine.performStateChangeMachineTransition(typeId, 0, web3.toHex('SHIPPED_OUT_TO_PRODUCER'), typeId, 100, shipState, stateMachine.address, {from: shipperAdmin});
      console.log('Transaction hash');
      console.log(tx.tx);
      console.log('Transaction receipt');
      console.log(web3.eth.getTransactionReceipt(tx.tx));
      const events = abiDecoder.decodeLogs(tx.receipt.logs);
      console.log('Decoded logs');
      console.log(events);
      console.log('Events from logs');
      console.log(events[0].events);

    })

  })


});
