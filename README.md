# Adding a state change

Transaction hash

`0x7bcec3f90eb5c8420f32af821fdc31cb8396bb9d88c8582ea284575fd60381c3`

Transaction receipt

```{ transactionHash: '0x7bcec3f90eb5c8420f32af821fdc31cb8396bb9d88c8582ea284575fd60381c3',
  transactionIndex: 0,
  blockHash: '0x5af59cf42691e8b5a2ab6545693fed1ab754121d6dc8d90bc15b8cbd3f5c9635',
  blockNumber: 4,
  gasUsed: 88826,
  cumulativeGasUsed: 88826,
  contractAddress: null,
  logs: 
   [ { logIndex: 0,
       transactionIndex: 0,
       transactionHash: '0x7bcec3f90eb5c8420f32af821fdc31cb8396bb9d88c8582ea284575fd60381c3',
       blockHash: '0x5af59cf42691e8b5a2ab6545693fed1ab754121d6dc8d90bc15b8cbd3f5c9635',
       blockNumber: 4,
       address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10',
       data: '0x4241434f4e000000000000000000000000000000000000000000000000000000524543454956455f534849504d454e540000000000000000000000000000000050524f434553535f534849504d454e5400000000000000000000000000000000000000000000000000000000627306090abab3a6e1400e9345bc60c78a8bef57',
       topics: [Object],
       type: 'mined' } ] }```
       
Decoded logs

```{ name: 'StateChangeAdded',
  events: 
   [ { name: 'productTypeId',
       type: 'bytes32',
       value: '0x4241434f4e000000000000000000000000000000000000000000000000000000' },
     { name: 'currState',
       type: 'bytes32',
       value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
     { name: 'nextState',
       type: 'bytes32',
       value: '0x50524f434553535f534849504d454e5400000000000000000000000000000000' },
     { name: 'user',
       type: 'address',
       value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ],
  address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10' }```
  
Events from logs

```[ { name: 'productTypeId',
    type: 'bytes32',
    value: '0x4241434f4e000000000000000000000000000000000000000000000000000000' },
  { name: 'currState',
    type: 'bytes32',
    value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
  { name: 'nextState',
    type: 'bytes32',
    value: '0x50524f434553535f534849504d454e5400000000000000000000000000000000' },
  { name: 'user',
    type: 'address',
    value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ]```
    
# Removing a state change

Transaction hash

`0xc89824524c74306bded1f8d0aa1b98b0396cf6eefda31b6c8be7fb7250927969`

Transaction receipt

```{ transactionHash: '0xc89824524c74306bded1f8d0aa1b98b0396cf6eefda31b6c8be7fb7250927969',
  transactionIndex: 0,
  blockHash: '0x128b1ddcccc990c23468b2116043cb4931bc0e13a0f5d7863f83413fba05a1a4',
  blockNumber: 5,
  gasUsed: 21214,
  cumulativeGasUsed: 21214,
  contractAddress: null,
  logs: 
   [ { logIndex: 0,
       transactionIndex: 0,
       transactionHash: '0xc89824524c74306bded1f8d0aa1b98b0396cf6eefda31b6c8be7fb7250927969',
       blockHash: '0x128b1ddcccc990c23468b2116043cb4931bc0e13a0f5d7863f83413fba05a1a4',
       blockNumber: 5,
       address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10',
       data: '0x4241434f4e000000000000000000000000000000000000000000000000000000524543454956455f534849504d454e540000000000000000000000000000000050524f434553535f534849504d454e5400000000000000000000000000000000000000000000000000000000627306090abab3a6e1400e9345bc60c78a8bef57',
       topics: [Object],
       type: 'mined' } ] }```
       
Decoded logs

```{ name: 'StateChangeRemoved',
  events: 
   [ { name: 'productTypeId',
       type: 'bytes32',
       value: '0x4241434f4e000000000000000000000000000000000000000000000000000000' },
     { name: 'currState',
       type: 'bytes32',
       value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
     { name: 'nextState',
       type: 'bytes32',
       value: '0x50524f434553535f534849504d454e5400000000000000000000000000000000' },
     { name: 'user',
       type: 'address',
       value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ],
  address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10' }```
  
Events from logs

```[ { name: 'productTypeId',
    type: 'bytes32',
    value: '0x4241434f4e000000000000000000000000000000000000000000000000000000' },
  { name: 'currState',
    type: 'bytes32',
    value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
  { name: 'nextState',
    type: 'bytes32',
    value: '0x50524f434553535f534849504d454e5400000000000000000000000000000000' },
  { name: 'user',
    type: 'address',
    value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ]```
    
# Performing a state change
    
Transaction hash

`0x1975fd5cb5d3d2cdd95ef4d879a871fcb43728d41fc141fa153ac0aec1cdc291`

Transaction receipt

```{ transactionHash: '0x1975fd5cb5d3d2cdd95ef4d879a871fcb43728d41fc141fa153ac0aec1cdc291',
  transactionIndex: 0,
  blockHash: '0x2da45a4ec07a57d7cae60245a006269e2929302ef89c30e118797ec77aec084e',
  blockNumber: 7,
  gasUsed: 46570,
  cumulativeGasUsed: 46570,
  contractAddress: null,
  logs: 
   [ { logIndex: 0,
       transactionIndex: 0,
       transactionHash: '0x1975fd5cb5d3d2cdd95ef4d879a871fcb43728d41fc141fa153ac0aec1cdc291',
       blockHash: '0x2da45a4ec07a57d7cae60245a006269e2929302ef89c30e118797ec77aec084e',
       blockNumber: 7,
       address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10',
       data: '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000524543454956455f534849504d454e5400000000000000000000000000000000000000000000000000000000627306090abab3a6e1400e9345bc60c78a8bef57',
       topics: [Object],
       type: 'mined' } ] }```
       
Decoded logs

```{ name: 'StateChanged',
  events: 
   [ { name: 'cycleId',
       type: 'bytes32',
       value: '0x0000000000000000000000000000000000000000000000000000000000000000' },
     { name: 'prevState',
       type: 'bytes32',
       value: '0x0000000000000000000000000000000000000000000000000000000000000000' },
     { name: 'newState',
       type: 'bytes32',
       value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
     { name: 'user',
       type: 'address',
       value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ],
  address: '0x345ca3e014aaf5dca488057592ee47305d9b3e10' }```
  
Events from logs

```[ { name: 'cycleId',
    type: 'bytes32',
    value: '0x0000000000000000000000000000000000000000000000000000000000000000' },
  { name: 'prevState',
    type: 'bytes32',
    value: '0x0000000000000000000000000000000000000000000000000000000000000000' },
  { name: 'newState',
    type: 'bytes32',
    value: '0x524543454956455f534849504d454e5400000000000000000000000000000000' },
  { name: 'user',
    type: 'address',
    value: '0x627306090abab3a6e1400e9345bc60c78a8bef57' } ]```